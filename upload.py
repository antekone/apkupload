import argparse
import sys
import os

import googleapiclient
from apiclient import sample_tools
from oauth2client import client
from google.oauth2 import service_account


def get_bundle_list(lst):
    if 'kind' not in lst or lst['kind'] != 'androidpublisher#bundlesListResponse':
        print("Invalid type of the return object, failing")
        sys.exit(1)

    return lst['bundles']


def continue_with_edit(s, pn: str, id: str, aab_file: str):
    print(f"Session {id}: Searching for existing Application Bundles...")

    bundles = get_bundle_list(s.edits().bundles().list(packageName=pn, editId=id).execute())
    max_vcode = 0
    for bundle in bundles:
        max_vcode = max(max_vcode, bundle['versionCode'])

    print(f"Session {id}: Highest version code: {max_vcode}")
    if max_vcode == 0:
        print(f"Session {id}: Hmm... Is this a first upload?")

    aab_response = s.edits().bundles().upload(
        packageName=pn,
        editId=id,
        media_body=aab_file,
        media_mime_type="application/octet-stream"
    ).execute()

    if 'versionCode' not in aab_response:
        print("Invalid AAB upload response. AAB bundle has NOT been uploaded, failing the process.")
        sys.exit(1)

    version_code = aab_response['versionCode']
    print(f"Session {id}: Uploaded version {version_code}")

    print(f"Session {id}: Committing...")
    commit_response = s.edits().commit(packageName=pn, editId=id).execute()

    # Would throw if it were an error.
    print(f"Session {id}: Commit OK.")


def main_sa(argv: list[str]):
    home_dir = os.getenv("HOME")
    sa_json = os.path.join(home_dir, ".config/org.antoniak.mobile/sa.json")
    if not os.path.isfile(sa_json):
        print("Missing the service account secret info. Please populate this file and retry:")
        print(f"- {sa_json}")
        sys.exit(1)

    argparser = argparse.ArgumentParser(add_help=False)
    argparser.add_argument('package_name',
                           help='The package name. Example: com.android.sample')

    argparser.add_argument('aab_file',
                           nargs='?',
                           default='app-release.aab',
                           help='The path to the AAB file to upload.')

    args = argparser.parse_args(argv[1:])

    if args.package_name is None:
        print("Missing the package name. Use `-h` to get help.")
        sys.exit(1)

    if args.aab_file is None:
        print("Missing a path to the AAB file. Use `-h` to get help.")
        sys.exit(1)

    if not os.path.isfile(args.aab_file):
        print("The file specified in the argument does not appear to be a valid file.")
        print(f"Argument: {args.aab_file}")
        sys.exit(1)

    creds = service_account.Credentials.from_service_account_file(sa_json, scopes=['https://www.googleapis.com/auth/androidpublisher'])
    service = googleapiclient.discovery.build('androidpublisher', 'v3', credentials=creds)

    package_name = args.package_name
    aab_file = args.aab_file

    try:
        print("Creating session context for:")
        print(f"- Package name: {package_name}")
        print(f"- Application bundle: {aab_file}")

        edit = service.edits().insert(packageName=package_name).execute()
        if 'id' not in edit:
            print("Can't create session context.")
            sys.exit(1)

        continue_with_edit(service, package_name, edit['id'], aab_file)
    except googleapiclient.errors.HttpError as e:
        print(f"Error while interacting with the Android Publisher API for package {package_name}.")
        print(f"Details: {e.reason}")
        sys.exit(1)


if __name__ == '__main__':
    main_sa(sys.argv)